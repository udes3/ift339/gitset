//
//  set2.h
//  SkipList
//
//  Jean Goulet 2022
//

#ifndef SkipList_set2_h
#define SkipList_set2_h

/////////////////////////////////////////////////////////////////
// copieur et destructeur de liste

template <typename TYPE>
set<TYPE>::set(const set<TYPE>& src):set(){
    for (auto& x : src) {
        insert(end(), x);
    }
}

template <typename TYPE>
set<TYPE>::~set(){
    if (AVANT != nullptr) {
        clear();
        delete AVANT->PREC[0];
        delete AVANT;
        AVANT = nullptr;
    }
}

/////////////////////////////////////////////////////////////////
// find

template <typename TYPE>
typename set<TYPE>::iterator set<TYPE>::find(const TYPE& VAL) {
    iterator pos(lower_bound(VAL));
    if (pos.POINTEUR->CONTENU == nullptr || !(*pos < VAL) && !(VAL<*pos))
        return pos;
    else
        return pos = AVANT->PREC[0];
}

// lower_bound et upper_bound

template <typename TYPE>
typename set<TYPE>::iterator set<TYPE>::lower_bound(const TYPE& VAL){
    cellule* av = AVANT;
    size_t i = AVANT->SUIV.size();
    while (i > 0){
        --i;
        while (av->SUIV[i] != AVANT->PREC[0] && *av->SUIV[i]->CONTENU < VAL) {
            av = av->SUIV[i];
        }
    }
    return iterator(av->SUIV[i]);
}

template <typename TYPE>
typename set<TYPE>::iterator set<TYPE>::upper_bound(const TYPE& VAL){
    iterator pos(lower_bound(VAL));
    if (pos != end() && !(*pos < VAL) && !(VAL < *pos)){
       return ++pos;
    }
   
    return pos;
}

/////////////////////////////////////////////////////////////////
// erase(VAL)

template <typename TYPE>
size_t set<TYPE>::erase(const TYPE& VAL){
    iterator pos(find(VAL));
    if (pos == end())
        return 0;
    erase(pos);
    return 1;    
}


// erase(it)
// elimine de l'ensemble l'element en position it

template <typename TYPE>
typename set<TYPE>::iterator set<TYPE>::erase(iterator it){
    size_t i = it.POINTEUR->PREC.size();
    while (i > 0) {
        --i;
        cellule* av = it.POINTEUR->PREC[i];
        cellule* ap = it.POINTEUR->SUIV[i];

        av->SUIV[i] = ap;
        ap->PREC[i] = av;
    }

    i = it.POINTEUR->PREC.size() - 1;
    while (i > 0) {
        if (it.POINTEUR->PREC[i] == AVANT && it.POINTEUR->SUIV[i] == AVANT->PREC[0]) {
            AVANT->SUIV.pop_back();
            AVANT->PREC[0]->PREC.pop_back();
        }
        i--;
    }

    delete it.POINTEUR;
    SIZE--;
    return it;
}


// clear
// vide le set en temps O(n)

template <typename TYPE>
void set<TYPE>::clear(){
    while(!empty()) {
        erase(begin());
    }
}


#endif
