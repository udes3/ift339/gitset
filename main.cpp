#include <iostream>
#include "set.h"
#include "fuite.h"

using namespace std;

int main(int argc, const char* argv[]) {
    OBJET O;
    set<OBJET> S2;

    set<OBJET> S = { 86,9,72,79,58,89,114,121,2,16,75 };
    S.afficher("set de base");

    cout << "Test lower_bound" << endl;
    cout << *(S.lower_bound(89));
    cout << endl << *(S.lower_bound(115));

    cout << endl << "Test upper_bound" << endl;
    cout << *(S.upper_bound(89)) << endl;
    cout << *(S.upper_bound(115)) << endl;
    
    cout << "Test copieur" << endl;
    S2.operator=(S);
    S2.afficher("Copie");

    cout << "Test destructeur" << endl;
    S2.clear();
    S2.afficher("Copie detruite");

    S2.insert(3);
    S2.insert(73);
    S2.insert(115);
    S2.insert(100);
    S2.afficher("Copie avec ajout de 3 elements");

    cout << endl << *(S.find(89)) << " le find de 89\n";
    if (S.find(90) == S.end())
        cout << "Le find est a la fin\n";
    else
        cout << "Le find n'est pas a la fin\n";

    S.erase(72);
    S.erase(73);
    S.afficher("Test erase valeur");

    cout << O.nb_comparaisons() << " comparaisons pour ces insertions" << endl;
    cout << endl << "Fin du programme" << endl;
    return 0;
}