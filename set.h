/*
 *  SkipList
 *
 *  Jean Goulet 2016
 *  Cette version integre la rustine
 *  set<TYPE>::to_string pour remplacer std::to_string
 *
 */

#pragma once

#include <iostream>
#include <sstream>
#include <cstdlib>
#include <vector>
#include <string>
#include <random>
#include <chrono>

using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::make_pair;

template <typename TYPE>
class set{
private:
	struct cellule{
		TYPE* CONTENU;
        vector<cellule*> PREC,SUIV;
        cellule(TYPE*C=nullptr):CONTENU(C){}
        ~cellule();
		};
	size_t SIZE;
	cellule* AVANT;
	
    static size_t tirer_couches_au_hasard(size_t);
    cellule* insert(cellule*,const TYPE&);
    bool est_plus_petit(const TYPE*,const TYPE*)const;
public:
    class iterator;

	set();
    set(std::initializer_list<TYPE>);
	~set();
	set(const set&);
	set& operator=(const set&);
    void swap(set&);
	
	size_t size()const;
	bool empty()const;
    
	size_t count(const TYPE&)const;
    iterator find(const TYPE&);
    iterator lower_bound(const TYPE&);
    iterator upper_bound(const TYPE&);
    
    std::pair<iterator,bool> insert(const TYPE&);
    iterator insert(iterator,const TYPE&);
	size_t erase(const TYPE&);
	iterator erase(iterator);
	void clear();
	
	iterator begin()const;
	iterator end()const;
	
    //rustine pour accommoder les nouvelles versions de C++
    template < typename T > static string to_string( const T& n ){
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
	void afficher(string="")const;
};

/////////////////////////////////////////////////
// destructeur de cellule

template <typename TYPE>
set<TYPE>::cellule::~cellule(){
    delete CONTENU;
    CONTENU=nullptr;
    for(auto& x:PREC)x=nullptr;
    for(auto& x:SUIV)x=nullptr;
}

//comparateur de TYPE*
template <typename TYPE>
bool set<TYPE>::est_plus_petit(const TYPE* gauche,const TYPE* droite)const{
    if(droite==nullptr)return true; //*gauche < +infini
    if(gauche==nullptr)return true; //-infini < *droite
    return (*gauche)<(*droite);
}

/////////////////////////////////////////////////
// iterator

template <typename TYPE>
class set<TYPE>::iterator{
private:
    cellule* POINTEUR;
public:
    friend class set<TYPE>;
    iterator(cellule*p=nullptr):POINTEUR(p){}
    const TYPE& operator*()const{return *POINTEUR->CONTENU;}
    iterator operator++();     //++i
    iterator operator++(int);  //i++
    iterator operator--();     //--i
    iterator operator--(int);  //i--
    bool operator==(const iterator&i2)const{return POINTEUR==i2.POINTEUR;}
    bool operator!=(const iterator&i2)const{return POINTEUR!=i2.POINTEUR;}
};


template <typename TYPE>
typename set<TYPE>::iterator set<TYPE>::iterator::operator++(){   //++i
    POINTEUR=POINTEUR->SUIV[0];
    return *this;
}

template <typename TYPE>
typename set<TYPE>::iterator set<TYPE>::iterator::operator++(int){ //i++
    iterator r(POINTEUR);
    POINTEUR=POINTEUR->SUIV[0];
    return r;
}

template <typename TYPE>
typename set<TYPE>::iterator set<TYPE>::iterator::operator--(){    //--i
    POINTEUR=POINTEUR->PREC[0];
    return *this;
}

template <typename TYPE>
typename set<TYPE>::iterator set<TYPE>::iterator::operator--(int){ //i--
    iterator r(POINTEUR);
    POINTEUR=POINTEUR->PREC[0];
    return r;
}

/////////////////////////////////////////////////
// set
// fonctions privees

template <typename TYPE>
size_t set<TYPE>::tirer_couches_au_hasard(size_t MAX){
    //tirer au hasard le nombre de couches
    //prob 1/2 d'avoir 1
    //prob 1/4 d'avoir 2
    //prob 1/8 d'avoir 3 etc.
    static auto seed = std::chrono::system_clock::now().time_since_epoch().count();
    static std::minstd_rand0 generator(static_cast<unsigned int>(seed));
    size_t i=1;
    auto g=generator();
    for(;g%2!=0 && i<=MAX;++i)g/=2;
    return i;
}


/////////////////////////////////////////////////
// set
// fonctions publiques

template <typename TYPE>
set<TYPE>::set(){
    SIZE=0;
    AVANT=new cellule(nullptr);
    cellule* APRES=new cellule(nullptr);
    AVANT->PREC.push_back(APRES);
    AVANT->SUIV.push_back(APRES);
    APRES->PREC.push_back(AVANT);
    APRES->SUIV.push_back(nullptr);
	}

template <typename TYPE>
set<TYPE>::set(std::initializer_list<TYPE> droite):set(){
    for(const auto& x:droite)
        insert(x);
}

template <typename TYPE>
set<TYPE>& set<TYPE>::operator=(const set<TYPE>& source){
    if(this!=&source){
        set copie(source);
        swap(copie);
    }
    return *this;
}

template <typename TYPE>
void set<TYPE>::swap(set<TYPE>& src){
    std::swap(SIZE,src.SIZE);
    std::swap(AVANT,src.AVANT);
}

template <typename TYPE>
size_t set<TYPE>::size()const{
	return SIZE;
}

template <typename TYPE>
bool set<TYPE>::empty()const{
	return SIZE==0;
}

template <typename TYPE>
size_t set<TYPE>::count(const TYPE& t)const{
    auto it=find(t);
    if(it==end())
        return 0;
    else return 1;
	}

/////////////////////////////////////////////////
// les trois fonctions d'insertion
// deux fonctions publiques et une fonction privée

template <typename TYPE>
std::pair<typename set<TYPE>::iterator,bool> set<TYPE>::insert(const TYPE& VAL){
    //insertion par valeur seulement
    iterator it=lower_bound(VAL);
    TYPE* p=it.POINTEUR->CONTENU;
     if(p==nullptr || VAL<*p)
        return std::make_pair(iterator(insert(it.POINTEUR,VAL)),true);
    else
        return std::make_pair(it,false);
}

template <typename TYPE>
typename set<TYPE>::iterator set<TYPE>::insert(iterator it,const TYPE& VAL){
    //insertion avec indice a verifier
    //verifier que l'on est a la bonne place
    cellule* ap=it.POINTEUR;
    cellule* av=ap->PREC[0];
    TYPE* ava=av->CONTENU;
    if(ap->CONTENU!=nullptr && (*ap->CONTENU<VAL))
        return insert(VAL).first;
    else if(ava==nullptr)
        return iterator(insert(ap,VAL));
    else if(*ava<VAL)
        return iterator(insert(ap,VAL));
    else 
        return insert(VAL).first;
}

template <typename TYPE>
typename set<TYPE>::cellule* set<TYPE>::insert(typename set<TYPE>::cellule* ap,const TYPE& VAL){
    using namespace std;
    size_t NBNIV=AVANT->SUIV.size();
    cellule* APRES=AVANT->PREC[0];
    cellule* av=ap->PREC[0];
    cellule* nouv=new cellule(new TYPE(VAL));
    size_t nbniv=tirer_couches_au_hasard(AVANT->SUIV.size());
    for(size_t i=0;;){
        nouv->PREC.push_back(av);
        nouv->SUIV.push_back(ap);
        av->SUIV[i]=nouv;
        ap->PREC[i]=nouv;
        if(++i==nbniv)
            break; //on a insere le bon nombre de couches
        if(i==NBNIV){  //ajouter une couche vide
            AVANT->SUIV.push_back(ap=APRES);
            APRES->PREC.push_back(av=AVANT);
            ++NBNIV;
        }
        else{
            while(av->SUIV.size()==i)
                av=av->PREC.back();
            ap=av->SUIV[i];
        }
    }
    ++SIZE;
    return nouv;
}



/////////////////////////////////////////////////
// iteration

template <typename TYPE>
typename set<TYPE>::iterator set<TYPE>::begin()const{
    return iterator(AVANT->SUIV[0]);
}

template <typename TYPE>
typename set<TYPE>::iterator set<TYPE>::end()const{
    return iterator(AVANT->PREC[0]);
}

/////////////////////////////////////////////////
// affichage
// n'est fonctionnel que pour les types primitifs
// et les string (max 4 caracteres par element)

//conversion d'une valeur en string
//cas generique
template <typename TYPE>
string vers_string(TYPE VAL){
    return set<TYPE>::to_string(VAL);
}

//cas specialise double
template <>
string vers_string<double>(double VAL){
    string retour=set<double>::to_string(VAL);
    if(retour.find('.')==string::npos)return retour;
    for(auto i=retour.rbegin();i!=retour.rend();++i)
        if(*i=='0')retour.pop_back();
        else break;
    if(retour.back()=='.')retour.pop_back();
    return retour;
}

//cas specialise float
template <>
string vers_string<float>(float VAL){
    return vers_string(double(VAL));
}

//cas specialise string
template <>
string vers_string<string>(string VAL){
    return VAL;
}

template <typename TYPE>
void set<TYPE>::afficher(string st)const{
    using namespace std;
    cout<<endl;
	cout<<st<<endl;
    if(AVANT==nullptr){
        cout<<"le set a ete detruit"<<endl;
        return;
    }
    else if(size()==0){
        cout<<"le set est vide"<<endl;
        return;
    }
    cellule *dernier=AVANT->PREC[0]->PREC[0];
    size_t NBNIV=AVANT->SUIV.size();
    string une_clef,lignes,clefs;
    TYPE elem;
    cellule *p;
    string s="s  ";
    if(size()<2)s="  ";
    lignes=set<TYPE>::to_string(SIZE)+" element"+s;
    s="s  ";
    if(AVANT->SUIV.size()<2)s="  ";
    lignes+=set<TYPE>::to_string(AVANT->SUIV.size())+" couche"+s;
    
    size_t MIN=size()/2-1;
    if(MIN>10)MIN=10;
    if(MIN<10)MIN=10;
    size_t MAX=size()/2;
    if(MAX>10)MAX=size()-9;
	for(size_t nb=NBNIV+1;nb>0;){
        string clefs=" ";
        nb--;
		//afficher toute la couche nb
        cout<<lignes<<endl;
        lignes="|";
        int i=0;
        for(auto it=begin();it!=end();++it,++i){
            p=it.POINTEUR;
            if(p->SUIV.size()>nb){
                elem=*p->CONTENU;
                if(i<MIN || i>MAX){
                    une_clef="-----"+vers_string(elem);
                    clefs+=une_clef.substr(une_clef.size()-6,6);
                    lignes+="     |";
                }
                else if(i==MIN){
                    clefs+= "  / ";
                    lignes+="  \\ ";
                }
            }
            else{
                if(i<MIN || i>MAX){
                    clefs+="-----";
                    if(p==dernier)lignes+="     |";
                    else {lignes+="      ";clefs+="-";}
                }
                else if(i==MIN){
                    clefs+= "  / ";
                    lignes+="  \\ ";
                }
              }
            }
        cout<<clefs<<endl;
		}
	cout<<endl<<endl;
	}

////////////////////////////////////////////////////////////

#include "set2.h"
